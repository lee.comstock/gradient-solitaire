import { Color } from './Color'

export type ColorChip = {
  color: Color,
  selected: Boolean,
  highlighted: Boolean,
}
