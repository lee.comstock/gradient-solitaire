import { Cell } from './Cell'

export type Grid = {
  cells: Cell[][],
}
