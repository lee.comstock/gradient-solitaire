import { ColorChip } from './ColorChip'

export type Cell = {
  colorChip?: ColorChip,
  highlighted: Boolean,
}
