import { createStore } from 'vuex'
import { Grid } from '../types/Grid'
import { ColorChip } from '../types/ColorChip'

export const store = createStore({
  state: {
    grid:               {}        as Grid,
    selectedColorChip:  undefined as (ColorChip | undefined),
    score:              0         as number,
    moves:              0         as number,
    maxMoves:           0         as number,
    exp:                0         as number,
    expGoal:            0         as number,
    level:              1         as number,
    highScores:         []        as number[],
  },
  getters: {
    isBoardCleared(state) {
      for (let column of state.grid.cells) {
        for (let cell of column) {
          if (cell.colorChip) return false;
        }
      }
      return true;
    },
    totalScore(state) {
      return state.highScores.reduce((acc, score) => acc + score, 0);
    },
  },
  actions: {
    init(context) {
      context.commit('setHighScores');
      context.commit('createNewBoard');
    },
    refreshBoard(context) {
      if (context.getters.isBoardCleared) {
        context.commit('updateHighScore');
        context.commit('setLevel', { level: context.state.level + 1 });
      }
      setTimeout(() => {
        context.commit('createNewBoard');
      }, 200);
    },
    startNewLevel(context, { level }) {
      context.commit('setLevel', { level });
      setTimeout(() => {
        context.commit('createNewBoard');
      }, 200);
    },
    colorChipClicked(context, { chip, coordinates }) {
      chip.highlighted
      ? context.commit('clearColorChipRow', { coordinates })
      : context.commit('colorChipSelection', { chip });
      context.commit('clearHighlights');
      // check that clicked color chip is selected, if not end function
      if (!chip.selected) return;
      setTimeout(() => {
        context.commit('highlightColorChipRow', { coordinates });
        context.commit('highlightCells', { coordinates });
      });
    },
    emptyCellClicked(context, { emptyCell, coordinates }) {
      // check that clicked empty cell is highlighted, if not end function
      if (!emptyCell.highlighted) return;
      context.commit('moveColorChip', { emptyCell, coordinates });
      context.commit('clearHighlights');
      setTimeout(() => {
        context.commit('highlightColorChipRow', { coordinates });
        context.commit('highlightCells', { coordinates });
      });
    },
  },
  mutations: {
    setHighScores(state) {
      // set highscores based on local storage or default array of zeros
      state.highScores = JSON.parse(localStorage.getItem('highScores') || '[0,0,0,0,0,0,0,0,0,0]');
    },
    updateHighScore(state) {
      // set new highscore
      if (state.score > state.highScores[state.level - 1]) {
        state.highScores[state.level - 1] = state.score;
      }
      // unlock levels
      for (let i = state.highScores.length; i < state.level + 10; i ++) {
        state.highScores[i] = 0;
      }
      // save highscores to local storage
      localStorage.setItem('highScores', JSON.stringify(state.highScores));
    },
    createNewBoard(state) {
      state.score      = 0;
      state.exp        = 0;
      state.moves      = 2;
      state.maxMoves   = 2;
      let columns      = 10 + (Math.pow(state.level, 2) % 16);
      let rows         = 5  + (Math.pow(state.level, 3) % 11);
      state.expGoal    = Math.round(((rows * columns) / 20) * (1 + (state.level / 10)));
      state.grid.cells = Array.from({ length: columns }, () =>
        Array.from({ length: rows }, () => (
          {
            highlighted: false,
            colorChip: (Math.random() < 0.1) ? undefined : {
              color: Math.floor(Math.random() * 6),
              selected: false,
              highlighted: false,
            },
          }
        ))
      );
    },
    setLevel(state, { level }) {
      state.level = level;
    },
    colorChipSelection(state, { chip }) {
      // handle color chip selection
      if (chip.selected) {
        // deselect clicked color chip
        state.selectedColorChip = undefined;
        chip.selected = false;
      } else {
        // deselect previously selected color chip and select clicked color chip
        if (state.selectedColorChip) state.selectedColorChip.selected = false;
        state.selectedColorChip = chip;
        chip.selected = true;
      }
    },
    moveColorChip(state, { emptyCell, coordinates }) {
      // use up one move
      state.moves --;
      // find selected color chip and move to the new cell
      for (let x = 0; x < state.grid.cells.length; x ++) {
        for (let y = 0; y < state.grid.cells[x].length; y ++) {
          if (state.grid.cells[x][y].colorChip?.selected) {
            // move selected color chip to the new cell
            emptyCell.colorChip = state.grid.cells[x][y].colorChip;
            // remove selected color chip from the old cell
            state.grid.cells[x][y].colorChip = undefined;
            // if move is a jump then change color of the color chip you are jumping over
            if (!((coordinates.x - x) % 2) && !((coordinates.y - y) % 2)) {
              let chipJumpedOver = state.grid.cells
                [(coordinates.x + x) / 2][(coordinates.y + y) / 2].colorChip;
              chipJumpedOver!.color = (chipJumpedOver!.color + 1) % 6;
            }
            // done
            return;
          }
        }
      }
    },
    clearColorChipRow(state, { coordinates }) {
      // loop through all 8 directions to find the selected color chip and then clear that row
      for (let xdir = -1; xdir < 2; xdir ++) {
        for (let ydir = -1; ydir < 2; ydir ++) {
          // check for no direction
          if (!xdir && !ydir) continue;
          // store cells for this direction, starting with the clicked color chip cell
          let cellRow = [state.grid.cells[coordinates.x][coordinates.y]];
          // traverse grid from clicked color chip to find the selected color chip
          for (let steps = 1; ; steps ++) {
            // coordinates of cell visited in this step
            let xVisited = coordinates.x + (xdir * steps);
            let yVisited = coordinates.y + (ydir * steps);
            // check if cell visited is outside of grid
            if ( xVisited < 0
              || yVisited < 0
              || xVisited > state.grid.cells.length - 1
              || yVisited > state.grid.cells[0].length - 1) {
              break;
            }
            // cell visited in this step
            let cellVisited = state.grid.cells[xVisited][yVisited];
            // empty cell, move on to next direction
            if (!cellVisited.colorChip) break;
            // add cell to array of cells for this direction
            cellRow.unshift(cellVisited);
            // found the selected color chip
            if (cellVisited.colorChip.selected) {
              // clearing a row of two color chips is not legal, so selecting instead
              if (steps == 1) {
                state.selectedColorChip!.selected = false;
                state.selectedColorChip = state.grid.cells[coordinates.x][coordinates.y].colorChip;
                state.selectedColorChip!.selected = true;
                return
              };
              // refill moves for removing color chips
              state.moves = state.maxMoves;
              // remove color chips in row
              for (let i = 0; i < cellRow.length; i ++) {
                setTimeout(() => {
                  // remove color chip
                  cellRow[i].colorChip = undefined;
                  // increase score
                  state.score += i + 1;
                  // increase experience points
                  state.exp ++;
                  // increase max moves when reaching experience goal
                  if (state.exp == state.expGoal) {
                    state.exp = 0;
                    state.maxMoves ++;
                    state.moves = state.maxMoves;
                  }
                }, i * 50);
              }
              // set selected color chip back to undefined
              state.selectedColorChip = undefined;
              // row of color chips removed, end function
              return
            }
            // color chip is not highlighted, move on to next direction
            if (!cellVisited.colorChip.highlighted) break;
          }
        }
      }
    },
    clearHighlights(state) {
      // remove all highlights
      for (let column of state.grid.cells) {
        for (let cell of column) {
          cell.highlighted = false;
          if (cell.colorChip) cell.colorChip.highlighted = false;
        }
      }
    },
    highlightColorChipRow(state, { coordinates }) {
      // loop through all 8 directions to highlight color chips you can clear off the board
      for (let xdir = -1; xdir < 2; xdir ++) {
        for (let ydir = -1; ydir < 2; ydir ++) {
          // check for no direction
          if (!xdir && !ydir) continue;
          // color gradient offset, positive or negative depending on direction
          let colorSteps = 0;
          // array for storing color chips for this direction
          let colorChipRow = [];
          // traverse grid from selected color chip to try to find color gradients
          for (let steps = 1; ; steps ++) {
            // coordinates of cell visited in this step
            let xVisited = coordinates.x + (xdir * steps);
            let yVisited = coordinates.y + (ydir * steps);
            // check if cell visited is outside of grid
            if ( xVisited < 0
              || yVisited < 0
              || xVisited > state.grid.cells.length - 1
              || yVisited > state.grid.cells[0].length - 1) {
              break;
            }
            // color chip visited in this step
            let colorChipVisited = state.grid.cells[xVisited][yVisited].colorChip;
            // empty cell, move on to next direction
            if (!colorChipVisited) break;
            // check for next color of color gradient in negative direction
            if (colorSteps < 1
            && state.selectedColorChip!.color == (colorChipVisited.color + 1 - colorSteps) % 6) {
              colorSteps --;
              // add color chip to array for this direction
              colorChipRow.push(colorChipVisited);
            }
            // check for next color of color gradient in positive direction
            if (colorSteps > -1
            && colorChipVisited.color == (state.selectedColorChip!.color + 1 + colorSteps) % 6) {
              colorSteps ++;
              // add color chip to array for this direction
              colorChipRow.push(colorChipVisited);
            }
            // color gradient not in progress, move on to next direction
            if (Math.abs(colorSteps) != Math.abs(steps)) break;
          }
          // color chips to clear has to be at least the one selected plus two highlighted
          if (colorChipRow.length > 1) {
            for (let chip of colorChipRow) chip.highlighted = true;
          }
        }
      }
    },
    highlightCells(state, { coordinates }) {
      // if you have no moves, end function
      if (!state.moves) return;
      // loop through all 8 directions to highlight cells this color chip can jump to
      for (let xdir = -1; xdir < 2; xdir ++) {
        for (let ydir = -1; ydir < 2; ydir ++) {
          // check for no direction
          if (!xdir && !ydir) continue;
          // number of steps until color chip to jump over, zero if not yet found
          let jumpSteps = 0;
          // traverse grid from selected color chip to try to find possible jump
          for (let steps = 1; ; steps ++) {
            // jump not possible
            if (jumpSteps && steps > jumpSteps * 2) break;
            // coordinates of cell visited in this step
            let xVisited = coordinates.x + (xdir * steps);
            let yVisited = coordinates.y + (ydir * steps);
            // check if cell visited is outside of grid
            if ( xVisited < 0
              || yVisited < 0
              || xVisited > state.grid.cells.length - 1
              || yVisited > state.grid.cells[0].length - 1) {
              break;
            }
            // cell visited in this step
            let cellVisited = state.grid.cells[xVisited][yVisited];
            // check if single step is possible
            if (steps == 1 && !cellVisited.colorChip) cellVisited.highlighted = true;
            // before finding chip we are jumping over
            if (!jumpSteps) {
              // found the chip we are jumping over
              if (cellVisited.colorChip) jumpSteps = steps;
              // moving to next step
              continue;
            }
            // jump not possible
            if (cellVisited.colorChip) break;
            // found the empty cell you can jump to
            if (!cellVisited.colorChip && steps == jumpSteps * 2) {
              // highlight empty cell
              cellVisited.highlighted = true;
            }
          }
        }
      }
    },
  },
})
